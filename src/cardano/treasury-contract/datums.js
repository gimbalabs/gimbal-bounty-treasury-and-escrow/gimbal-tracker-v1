import Cardano from "../serialization-lib";
import { fromHex, toHex, fromStr } from "../../utils/converter";

export const serializeBountyDatum = ({ issuer, contributor, lovelaceAmount, tokenAmount, expirationTime }) => {
  const fields = Cardano.Instance.PlutusList.new();

  console.log("BUILDING BOUNTY DATUM")
  console.log("Issuer Address", issuer)
  console.log("contributor Address", contributor)

  fields.add(Cardano.Instance.PlutusData.new_bytes(fromHex(issuer)));
  fields.add(Cardano.Instance.PlutusData.new_bytes(fromHex(contributor)));
  fields.add(
    Cardano.Instance.PlutusData.new_integer(
      Cardano.Instance.BigInt.from_str(`${lovelaceAmount}`)
    )
  );
  fields.add(
    Cardano.Instance.PlutusData.new_integer(
      Cardano.Instance.BigInt.from_str(`${tokenAmount}`)
    )
  );
  fields.add(
    Cardano.Instance.PlutusData.new_integer(
      Cardano.Instance.BigInt.from_str(`${expirationTime}`)
    )
  );

  const datum = Cardano.Instance.PlutusData.new_constr_plutus_data(
    Cardano.Instance.ConstrPlutusData.new(
      Cardano.Instance.Int.new_i32(0),
      fields
    )
  );

  return datum;
};

export const serializeTreasuryDatum = ({ bountyCount, treasuryKey }) => {
  const fields = Cardano.Instance.PlutusList.new();

  fields.add(
    Cardano.Instance.PlutusData.new_integer(
      Cardano.Instance.BigInt.from_str(`${bountyCount}`)
    )
  );
  fields.add(Cardano.Instance.PlutusData.new_bytes(treasuryKey));

  const datum = Cardano.Instance.PlutusData.new_constr_plutus_data(
    Cardano.Instance.ConstrPlutusData.new(
      Cardano.Instance.Int.new_i32(0),
      fields
    )
  );

  return datum;
};

// Do we need these?
// In Unsigs marketplace, we used deserializeOffer to add metadata as backup to Tx

export const deserializeBounty = (datum) => {
  const details = datum.as_constr_plutus_data().data();

  return {
    issuer: toHex(details.get(0).as_bytes()),
    contributor: toHex(details.get(1).as_bytes()),
    lovelaceAmount: details.get(2).as_integer().to_str(),
    tokenAmount: details.get(3).as_integer().to_str(),
    expirationTime: details.get(4).as_integer().to_str(),
  };
};

export const deserializeTreasury = (datum) => {
  const details = datum.as_constr_plutus_data().data();

  return {
    bountyCount: details.get(0).as_integer().to_str(),
    issuer: toHex(details.get(1).as_bytes()),
  };
};