import React, { useEffect, useState } from "react";
import { StaticQuery, graphql, Link } from "gatsby";
import { Heading, Text, Box, Flex, Button } from "@chakra-ui/react";

import { useStoreActions, useStoreState } from "easy-peasy";
import useWallet from "../hooks/useWallet";

import { fromHex, toStr } from "../utils/converter";
import { createBountyDatum } from "../utils/factory";
import { accessPolicyID } from "../cardano/treasury-contract";
import { distributeBounty } from "../cardano/bounty-contract";

const AccessTokens = () => {

    const connected = useStoreState((state) => state.connection.connected);
    const accessTokens = useStoreState((state) => state.accessTokens.tokenNames);
    const setAccessTokens = useStoreActions((actions) => actions.accessTokens.setAccessTokens);
    const { wallet } = useWallet(null)
    const [walletAddress, setWalletAddress] = useState(null)

    useEffect(async () => {
        if (connected && wallet) {
            const myAssets = await wallet.assets;
            const myAccessTokens = myAssets.filter(asset => asset.substring(0, 56) == accessPolicyID)
            setAccessTokens(myAccessTokens);
            setWalletAddress(wallet.address)
        }
    }, [wallet])

    return (
        <Flex p='3' mx='auto' bg="gl-blue" color="white">
            {accessTokens?.length >= 1 ? (
                <Box>
                    <Heading size='md'>
                        You have {accessTokens?.length > 1 ? `${accessTokens.length} Access Tokens` : "one Access Token"}:
                    </Heading>
                    {accessTokens?.map(asset => {
                        const assetString = asset.substring(56)
                        const assetName = toStr(fromHex(assetString))
                        return (
                            <Text>{assetName}</Text>
                        )
                    })}
                </Box>
            ) : (
                <Box>
                    <Heading size='md'>You do not hold any Access Tokens</Heading>
                    <Text>In order to commit to one of these bounties, you will need an Access Token.</Text>
                    <Text>To get one, send a DM to James on Discord. (Note: this is a temporary, quite centralized solution.)</Text>
                </Box>
            )}

        </Flex>
    )
}

export default AccessTokens