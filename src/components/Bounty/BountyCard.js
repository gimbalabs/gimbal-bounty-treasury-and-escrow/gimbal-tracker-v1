import React, { useEffect, useState } from "react";
import { StaticQuery, graphql, Link } from "gatsby";
import {
    Heading, Text, Box, Flex, Spacer, Badge,
    Stat, StatLabel, StatNumber, StatGroup, Divider, Tag,
} from "@chakra-ui/react";

const BountyCard = (props) => {
    const frontmatter = props.info

    let bountyColor = 'gl-blue'
    if (frontmatter.tags.includes("Committed")) { bountyColor = 'red.200' }
    if (frontmatter.tags.includes("Complete")) { bountyColor = 'green.800' }
    if (frontmatter.tags.includes("Priority")) { bountyColor = 'purple.800' }

    return (
        <Link to={frontmatter.slug}>
            <Flex direction='column' w='100%' h='100%' border='1px' borderBottom='0px' borderColor={bountyColor} key={frontmatter.slug}>
                <Flex direction='row' my='2'>
                    <Box w='70%' h='24' p='2' borderRight='2px'>
                        <Text fontSize='lg' fontWeight='bold'>
                            {frontmatter.title}
                        </Text>
                    </Box>
                    <StatGroup>
                        <Stat mx='4'>
                            <StatNumber>{frontmatter.gimbals}</StatNumber>
                            <StatLabel>gimbals</StatLabel>
                        </Stat>

                        <Stat mx='4'>
                            <StatNumber>{frontmatter.ada}</StatNumber>
                            <StatLabel>ADA</StatLabel>
                        </Stat>
                    </StatGroup>
                </Flex>
                <Flex direction='row' p='2' bg={bountyColor} color='white'>
                    <Tag colorScheme='orange'>
                        {frontmatter.slug}
                    </Tag>
                    <Spacer />
                    <Text>
                        {frontmatter.date}
                    </Text>
                    <Spacer />
                    <Text>
                        {frontmatter.scope}
                    </Text>
                    <Spacer />
                    {frontmatter.tags.map(tag => <Badge m='1'>{tag}</Badge>)}
                </Flex>
            </Flex>
        </Link>
    )
}

export default BountyCard