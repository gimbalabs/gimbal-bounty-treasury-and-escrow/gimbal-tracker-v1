import React, { useEffect, useState } from "react";
import { StaticQuery, graphql, Link } from "gatsby";
import {
    Heading, Text, Box, Flex, Button,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react";

import { useStoreState } from "easy-peasy";
import useWallet from "../../hooks/useWallet";
import { useTxAddresses } from "../../hooks/useTxAddresses";

import { fromHex, toHex, toStr } from "../../utils/converter";
import { createBountyDatum } from "../../utils/factory";
import { distributeBounty } from "../../cardano/bounty-contract";
import { accessPolicyID, bountyTokenUnit, treasuryContractAddress, treasuryIssuerAddress } from "../../cardano/treasury-contract";


const CommittedBounty = (props) => {

    const connected = useStoreState((state) => state.connection.connected);
    const { wallet } = useWallet(null)
    const [walletUtxos, setWalletUtxos] = useState([])
    const [walletAddress, setWalletAddress] = useState(null)
    const [contributorAddress, setContributorAddress] = useState(null)
    const [successfulTxHash, setSuccessfulTxHash] = useState(null)

    // The way array indexes are handled here is not build to scale.
    const utxo = props.utxo
    const slug = utxo.transaction.metadata[0].value
    const lovelace = utxo.value
    const bountyTxHash = utxo.transaction.hash
    const bountyTxIx = utxo.index

    const tokens = utxo.tokens //a list of token objects
    const gimbalsFromList = tokens.filter(i => i.asset.assetId == bountyTokenUnit)
    const gimbals = gimbalsFromList[0].quantity
    const accessTokenFromList = tokens.filter(i => i.asset.policyId == accessPolicyID)
    const accessTokenHex = accessTokenFromList[0].asset.assetName
    const accessToken = toStr(fromHex(accessTokenHex))

    // This hook gets the input Addresses from the Transaction whose output was the Bounty UTXO
    // One input address is the Treasury, so we can disregard that.
    // The other input address is the Contributor
    const {
        txAddresses,
        getTxAddresses,
        loading: addressesLoading,
        error,
    } = useTxAddresses();

    // To get it working, just hard code the Contributor Address:


    // For Modal:
    const { isOpen: isSuccessOpen, onOpen: onSuccessOpen, onClose: onSuccessClose } = useDisclosure()
    const { isOpen: isErrorOpen, onOpen: onErrorOpen, onClose: onErrorClose } = useDisclosure()

    useEffect(async () => {
        if (connected && wallet) {
            await getTxAddresses({
                variables: {
                    txhash: bountyTxHash
                },
            });
            const myUtxos = await wallet.utxos;
            setWalletUtxos(myUtxos);
            setWalletAddress(wallet.address)
        }
    }, [wallet])

    useEffect(async () => {
        if (txAddresses) {
            const transactionAddresses = txAddresses.transactions[0]?.inputs.map(input => input.address)
            const contributorAddresses = transactionAddresses?.filter(address => address != treasuryContractAddress)
            if (contributorAddresses) {
                setContributorAddress(contributorAddresses[0])
            }
        }
    }, [txAddresses])

    console.log("Access Token", accessToken)
    console.log("Contributor to", slug, contributorAddress)
    console.log("Bounty amounts", lovelace, gimbals)

    const handleDistribute = async () => {
        try {
            console.log("DISTRIBUTE A BOUNTY", lovelace, gimbals)
            const bDatum = createBountyDatum(connected, contributorAddress, lovelace, gimbals, 10000000000)
            const bUtxo = {
                "tx_hash": bountyTxHash,
                "output_index": bountyTxIx,
                "amount": [
                    { "unit": "lovelace", "quantity": `${lovelace}` },
                    { "unit": `${bountyTokenUnit}`, "quantity": `${gimbals}` },
                    { "unit": `${accessPolicyID}${accessTokenHex}`, "quantity": "1" }
                ],
            }

            console.log("bounty UTXO:", bUtxo)

            const bountyDistribution = {
                issuerAddress: connected,
                contributorAddress,
                utxosParam: walletUtxos,
                slug,
                accessTokenName: accessToken,
                lovelace,
                gimbals,
            }

            const txHash = await distributeBounty(bUtxo, bDatum, bountyDistribution)
            console.log("txHash is", txHash)
            if(txHash.error) throw "there was an error"
            else {
                setSuccessfulTxHash(txHash);
                onSuccessOpen();
            }

        } catch (error) {
            onErrorOpen();
        }

    }

    return (
        <Flex direction='column' w='100%' p='3' m='3' bg="red.200" key={slug}>
            <Box>
                <Link to={`/bounties/${slug}`}>
                    <Heading size='md'>
                        Committed Bounty: {slug}
                    </Heading>
                </Link>
                <Text fontSize='xs' py='1'>
                    {bountyTxHash}#{bountyTxIx}
                </Text>
                <Text fontSize='xs' py='1'>
                    Contributor Address: {contributorAddress}
                </Text>
            </Box>
            <Flex direction='row' justifyContent='center'>
                <Box bg='blue.200' m='3' p='3' fontSize='sm'>
                    Ada: {lovelace / 1000000}
                </Box>
                <Box bg='blue.200' m='3' p='3' fontSize='sm'>
                    {/* Testnet */}
                    Gimbals: {gimbals}
                    {/* Mainnet */}
                    {/* Gimbals: {gimbals / 1000000} */}
                </Box>
                <Box bg='blue.200' m='3' p='3' fontSize='sm'>
                    Access Token: {accessToken}
                </Box>
            </Flex>
            {connected == treasuryIssuerAddress ? (
                <Button m='3' onClick={handleDistribute}>Distribute this Bounty</Button>
            ) : ""}
            <Modal isOpen={isSuccessOpen} onClose={onSuccessClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Success!</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Heading size='sm'>You distributed the Bounty</Heading>
                        <Text>Next step: add successful txHash here: {JSON.stringify(successfulTxHash)}</Text>
                    </ModalBody>

                    <ModalFooter>
                        <Button colorScheme='blue' mr={3} onClick={onSuccessClose}>
                            Great!
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
            <Modal isOpen={isErrorOpen} onClose={onErrorClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Error!</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Heading size='sm'>There was an error with this tx</Heading>
                        <Text>We are working on extending the error reporting for this Dapp (see bounty!). For now, you can check the browser console to idenify the error.</Text>
                    </ModalBody>

                    <ModalFooter>
                        <Button colorScheme='blue' mr={3} onClick={onErrorClose}>
                            Dang!
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </Flex>
    )
}

export default CommittedBounty