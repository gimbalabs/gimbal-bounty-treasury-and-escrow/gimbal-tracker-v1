# Testnet Front-End Docs

## Project Variables:
- The Issuer's address is `addr_test1qz2h42hnke3hf8n05m2hzdaamup6edfqvvs2snqhmufv0eryqhtfq6cfwktmrdw79n2smpdd8n244z8x9f3267g8cz6s59993r`
- The Bounty Payment token is: `tpblTestGimbal`: `6c57132fde399c9ea6e462c4214d164984891087922b6fa2472b175b.7470626c5465737447696d62616c`
- In this instance, the Access Token PolicyID is: `5d6b6c332866044b2a8bdd147d92f77e42714986f1cb98cef70e201f`
- Bounty Treasury Contract: `addr_test1wql5wkfvdv0t605pwgaqj6zzpj65n50t8cxh5g5rapvrkhcvxr229`
- Bounty Escrow Contract: `addr_test1wryms9twwky535d2d8y9tkqke9lrlhg7hlm9dvnf2uk7tpcp07v2g`
- Bounty Escrow Validator Hash: `c9b8156e758948d1aa69c855d816c97e3fdd1ebff656b269572de587`

## Here is a list of files where (for now), you'll need to tweak some variables:
- `components/WalletButton`
- `src/cardano/bounty-contract`
    - `plutus.js`
- `src/cardano/treasury-contract`
    - `plutus.js`
    - `index.js`
- `src/cardano/locking-tx`
- `src/cardano/transaction/index.js`

## Steps:
### 0. In `gatsby-config.js` change the Dandelion URL to match testnet or mainnet as needed.

### 1. In `/src/components/WalletButton/WalletButton.jsx`, look for
```
if ((await window.cardano.getNetworkId()) === 0) return true;
```
- Testnet -> `0` | Mainnet -> `1`

### 2. In `src/cardano/bounty-contract`:
- In `plutus.js`:
    - Replace the `cborHex` with the string from compiled `_-bounty-escrow-vX.plutus`
### 3. In `src/cardano/treasury-contract`:
- In `plutus.js`:
    - Replace the `cborHex` with the string from compiled `_-bounty-treasury-vX.plutus`
- in `index.js` (this is the big one!)
    - Replace `// Project Instance Variables` with the corresponding variables above

### 4. In `templates/bountyPage.js`:
- look for where frontmatter is called at top of file; toggle between testnet and mainnet (comment in/out) to handle Gimbal decimal places on Mainnet.

### 5 (optional). In `src/cardano/transaction/index.js`:
- The transaction metadata key is hard-coded into this file. For now, search for `1618`, and change it if you'd like.