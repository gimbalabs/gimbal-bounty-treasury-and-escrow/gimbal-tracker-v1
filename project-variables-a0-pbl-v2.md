# Mainnet Deployment Guide

## Git Branch: `mainnet-pbl-a0-v2`

## Project Variables:
- The Issuer's address is: `addr1qxdgudzgf00ghdd8tw5ylylk4t76hsgm9pvr9ce73etppk0c8aursjfdhu7nr3sxujgczt2ndefwfc80pphdafv7fnrq7wk9fz`
- This issuers PKH is: `9a8e34484bde8bb5a75ba84f93f6aafdabc11b285832e33e8e5610d9`
- The Bounty Payment token is: `gimbal`: `2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30.67696d62616c`
- In this instance, the Access Token PolicyID is: `68ae22138b3c82c717713d850e5ee57c7de5de8591f5f13cd3a6cc67`
- Bounty Treasury Contract: `addr1w9k3kh6a2tjd24unl4wzgst66z2856wv5jxeh54rzgsjhfg56njxk`
- Bounty Escrow Contract: `addr1w9qmkhygyqkft984l59myj3fahl2hesc5gzfke7p783flvshcmpk8`
- Bounty Escrow Validator Hash: `41bb5c88202c9594f5fd0bb24a29edfeabe618a2049b67c1f1e29fb2`

## Here is a list of files where (for now), you'll need to tweak some variables:
- `gatsby-config.js`
- `components/WalletButton`
- `src/cardano/bounty-contract`
    - `plutus.js`
- `src/cardano/treasury-contract`
    - `plutus.js`
    - `index.js`
- `src/cardano/locking-tx`
- `src/cardano/transaction/index.js`

## Steps:
### 0. In `gatsby-config.js` change the Dandelion URL to match testnet or mainnet as needed.

### 1. In `/src/components/WalletButton/WalletButton.jsx`, look for
```
if ((await window.cardano.getNetworkId()) === 0) return true;
```
- Testnet -> `0` | Mainnet -> `1`

### 2. In `src/cardano/bounty-contract`:
- In `plutus.js`:
    - Replace the `cborHex` with the string from compiled `_-bounty-escrow-vX.plutus`
### 3. In `src/cardano/treasury-contract`:
- In `plutus.js`:
    - Replace the `cborHex` with the string from compiled `_-bounty-treasury-vX.plutus`
- in `index.js` (this is the big one!)
    - Replace `// Project Instance Variables` with the corresponding variables above

### 4. In `templates/bountyPage.js`:
- look for where frontmatter is called at top of file; toggle between testnet and mainnet (comment in/out) to handle Gimbal decimal places on Mainnet.

### 5 (optional). In `src/cardano/transaction/index.js`:
- The transaction metadata key is hard-coded into this file. For now, search for `1618`, and change it if you'd like.